from django.test import TestCase
from rest_framework.test import APIClient
import json


class APITest(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()

        self.user1_data = {
            "email": "luizmatheusga@gmail.com",
            "password": "1234Luiz!",
        }

    def test_create_and_login_for_user(self) -> None:
        # creating user
        user1 = self.client.post("/api/signup/", self.user1_data, format="json")

        # testing user's creation
        self.assertEqual(
            user1.json()["email"],
            self.user1_data["email"],
        )
        self.assertEqual(user1.status_code, 201)

        # testing user's login
        response = self.client.post(
            "/api/login/", self.user1_data, format="json"
        ).json()

        self.assertIn("token", response.keys())
        self.assertEqual(user1.status_code, 200)

    def test_create_and_login_for_wrong_user(self) -> None:
        # trying to create user with missing field
        user1 = self.client.post(
            "/api/signup/", self.user1_data["email"], format="json"
        )

        # testing user's error
        self.assertEqual(user1.status_code, 400)

        # testing user's login
        response = self.client.post(
            "/api/login/", self.user1_data["email"], format="json"
        ).json()

        self.assertEqual(response.status_code, 400)

    def test_user_can_just_get_own_information(self) -> None:
        # create a user
        self.client.post("/api/signup/", self.user1_data, format="json")

        token = self.client.post("/api/login/", self.user1_data, format="json").json()[
            "token"
        ]

        self.client.credentials(HTTP_AUTHORIZATION="Token " + token)

        request = self.client.get("/api/user/", format="json").json()

        self.assertEqual(len(request), 1)
