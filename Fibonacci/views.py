from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework import viewsets, status
from rest_framework.response import Response

from datetime import datetime, timezone, timedelta

from accounts.models import User
from accounts.serializers import UserSerializer
from Fibonacci.models import Fibonacci
from Fibonacci.serializers import FiboncciSerializer


class FibonacciView(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Fibonacci.objects.all()
    serializer_class = FiboncciSerializer
    lookup_field = "position"

    def create(self, request, *args, **kwargs):

        request.data["user"] = request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        fibonacci = Fibonacci.objects.get_or_create(**request.data)[0]

        serializer = FiboncciSerializer(fibonacci)

        headers = self.get_success_headers(serializer.data)

        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):

        partial = kwargs.pop("partial", False)

        instance = self.get_object()
        instance.user = request.user
        instance.last_update = datetime.now(timezone(offset=-timedelta(hours=3)))

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
