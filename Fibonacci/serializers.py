from rest_framework import serializers
from rest_framework.generics import get_object_or_404
from Fibonacci.models import Fibonacci
from accounts.serializers import UserSerializer
from accounts.models import User


class FiboncciSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Fibonacci

        fields = ["value", "position", "last_update", "user"]
