from django.conf.urls import url
from django.conf.urls import include
from Fibonacci.views import FibonacciView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("fibonacci", FibonacciView, basename="accounts")

urlpatterns = [url(r"", include(router.urls))]
