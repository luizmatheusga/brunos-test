from django.db import models
from datetime import datetime, timezone, timedelta
from accounts.models import User


class Fibonacci(models.Model):
    value = models.IntegerField()
    position = models.IntegerField(unique=True)
    last_update = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
