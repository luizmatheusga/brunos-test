from django.conf.urls import url
from django.conf.urls import include
from accounts.views import UserView, LoginView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("accounts", UserView, basename="accounts")
router.register("login", LoginView, basename="login")

urlpatterns = [url(r"", include(router.urls))]
