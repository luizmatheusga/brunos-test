from rest_framework import permissions


class UserPermision(permissions.BasePermission):
    def has_permission(self, request, _):

        if request.method == "POST":
            return True

        return bool(request.user and request.user.is_authenticated)
