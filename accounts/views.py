from rest_framework.generics import get_object_or_404
from accounts.models import User
from accounts.serializers import UserSerializer
from accounts.permissions import UserPermision

from django.contrib.auth import authenticate
from django.db.utils import IntegrityError
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework import mixins, viewsets
from rest_framework import status
from rest_framework.response import Response


class UserView(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (UserPermision,)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        User.objects.create_user(**request.data)

        headers = self.get_success_headers(serializer.data)

        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def list(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        if request.user.id != int(kwargs["pk"]):
            return Response(
                {"message": "Anauthorized"}, status=status.HTTP_401_UNAUTHORIZED
            )

        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, pk):
        if request.user.id != int(pk):
            return Response(
                {"message": "Anauthorized"}, status=status.HTTP_401_UNAUTHORIZED
            )

        user = get_object_or_404(User, id=request.user.id)

        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class LoginView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = AuthTokenSerializer

    def create(self, request):
        email = request.data["email"]
        password = request.data["password"]

        try:
            user = authenticate(email=email, password=password)

            if not user:
                raise User.DoesNotExist

            token = Token.objects.get_or_create(user=user)[0]

            return Response({"token": token.key}, status=status.HTTP_200_OK)
        except User.DoesNotExist:
            return Response(
                {"Message": "This user is not registered!"},
                status=status.HTTP_404_NOT_FOUND,
            )

        except IntegrityError:
            return Response(
                {"Message": "Unauthorized user!"}, status=status.HTTP_401_UNAUTHORIZED
            )
